package com.atlassian.http;

import javax.servlet.ServletRequest;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

/**
 * An abstract type safe, null safe api over top of javax servlet context management
 */
public class Context {

    /**
     * The context is stored inside the request object, s.t. Spring will manage it's lifecycle.
     */
    public static Context get(ServletRequest req) {
        Context reqCtx = (Context) req.getAttribute(Context.class.getName());
        if (reqCtx == null) {
            reqCtx = new Context();
            req.setAttribute(Context.class.getName(), reqCtx);
        }
        return reqCtx;
    }

    private final Map<String, Object> objects;

    public Context() {
        this.objects = new TreeMap<>();
    }

    public <T> Optional<T> get(Class<T> ctxType) {
        return Optional.ofNullable((T)objects.get(ctxType.getName()));
    }

    public <T> Context put(Class<T> type, T value){
        if(type == null || value == null) {
            return this;
        }

        objects.put(type.getName(), value);
        return this;
    }

    public <T> Context put(T value) {
        return put((Class<T>)value.getClass(), value);
    }

}
